puts "Holy giraffes! You fell into a maze!"
print "Where to? (Number of degrees, compass directions, up/down, or forward/backward):"
direction = gets.chomp.upcase

if (direction == "N" || direction == "North")
  place = "North"
elsif (direction == "E" || direction == "East")
  place = "East"
elsif (direction == "S" || direction == "South")
  place = "South"
elsif (direction == "W" || direction == "West")
  place = "West"
elsif (direction == "NW" || direction == "North West")
  place = "North West"
elsif (direction == "SW" || direction == "South West")
  place = "South West"
elsif (direction == "NE" || direction == "North East")
  place = "North East"
elsif (direction == "SE" || direction == "South East")
  place = "South East"

puts "#{place}, you say? A fine choice!"

if place == "North"
  puts "You are in a maze of twisty little passages, all alike."
elsif place == "East"
  puts "You see an elf! And his pet ham!"
elsif place == "South"
  puts "There's a minotaur! Wait, no, that's just your reflection."
elsif place == "West"
  puts "You're here, wherever here is."
elsif place == "North West"
  puts "Ahh! You have fallen into a hole!"
elsif place == "South West"
  puts "You see a proper young lad and his pet rock."
elsif place == "North East"
  puts "There are a thousand squids! All at once!"
elsif place == "South East"
  puts "You are in a slide."
else
  puts "Wait, is that even a direction?"

end
end

if (direction == "Up" || direction == "Down")
  puts "#{direction}, you say? A fine choice!"
else
  puts "Wait, is that even a direction?"
end

if direction == "Up"
  puts "You are on top of a hedge. Good job!"
elsif direction == "Down"
  puts "You are on a never ending staircase. Good luck!"
end
