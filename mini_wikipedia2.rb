information = {
  Muses: "The Muses are minor goddesses of the Greek pantheon. (Pantheon meaning a group of particularly respected, famous, or important people.) They serve as personifications of literary arts, music, visual arts, and science.
The Muses are:
Calliope: Epic Poetry
Clio: History
Euterpe: Music, Song, and Lyric Poetry
Erato: Love Poetry
Melopomene: Tragedy
Polyhymnia: Sacred Poetry
Terpsichore: Dance
Thalia: Comedy
Urania: Astronomy",
  BarnsleyFern: "The Barnsley fern is a fractal named after British mathematician Michael Barnsley who made it to resemble the black spleenwort. The fern is a basic example of what is called a self-similar set, i.e. it is a mathematically generated pattern that can be reproducible at any magnification or reduction.",
  Zoroastrianism: "Zoroastrianism is one of the world's oldest active religions, dating back to the 5th century BCE. It is a monotheistic faith that is centered around the concept of good and evil. It believes in an eschatology that predicts the ultimate destruction of evil. ",
  Eschatology: "Eschatology is the part of theology concerned with death, judgement, and the final destiny of the soul and of human kind.",
  Hydrography: "Hydrography is the science of surveying and charting bodies of water" ,
  BeatGeneration: "The Beat Generation was a literary movement that was popularized throughout the 1950s. The central elements of Beat culture are the rejection of standard narrative values, making a spiritual quest, the exploration of American and Eastern religions, the rejection of materialism, explicit portrayals of the human condition, experimentations with drugs, and sexual liberation and exploration. Allen Ginsberg's Howl(1956), William S. Burrough's Naked Lunch(1959), Jack Kerouac's On the Road(1957) and among the best known examples of Beat Literature.",
  Hedonsim: "Hedonism is a school of thought that argues that the pursuit of pleasure and intrinic good are the primary and most important goals of human life.",
  Bohemianism: "Bohemianism is the practice of an unconventional lifestyle, often in the company of like-minded people and with few permanent ties. It involves musical, artistic, literary or spiritual pursuits. The use of the word first appeared in the English language in the 19th century to describe the non-traditional lifestyles of marginalized and impoverished artists, writers, journalists, musicians, and actors in major European cities.",
  Rhapsodies: "A rhapsody in music is a one-movemnt work that is episodic yet integrated, free-flowing in structure, featuring a range of highly contrasted modds, colour, and tonality. An air of spontaneous inspiration and a sense of improvisation make it freer in form than a set a variations.",
  Rhapsodists: "A rhapsodist is a reciter of epic poetry.",
  JimMorrison: 1,
  
}

answer = 0
answer2 = 0
answer3 = 0

# area = information.keys.to_a.sample
# location = information.keys.to_a.sample
# place = information.keys.to_a.sample

while answer != "stop" && answer2 != "stop" && answer3 != "stop"
  area = information.keys.to_a.sample
  location = information.keys.to_a.sample
  place = information.keys.to_a.sample
  print "Do you know about #{area}? "
  answer = gets.chomp.downcase
    if answer == "yes"
      print "Do you know about #{location}? "
      answer2 = gets.chomp.downcase
      if answer2 == "yes"
        print "Do you know about #{place}?"
        answer3 = gets.chomp.downcase
        if answer3 == 'yes'
          print "Wow, you're clever!"
        elsif answer3 == "no"
          puts information[place]
        end
      elsif answer2 == "no"
        puts information[location]
      end
    elsif answer == "no"
      puts information[area]
    end
end
