hashery_menu = {
  eggs: 2,
  hash: 3,
  jam: 1,
  sausage: 2,
  biscuit: (1..3)
}

specials = {
  cheesyeggs: 5,
  pancakes: 7,
  waffles: 6
}


hashery_menu.keys.each do |item|
  puts "Today we're serving: #{item}!"
end

hashery_menu.each do |item, price|
  puts "We've got #{item} for $#{price}. What a deal!"
end

puts "hashery_menus what a biscuit'll run ya, depending on how much butter you want: "
hashery_menu[:biscuit].to_a.each do |price|
  puts "$#{price}"
end

hash_specials = specials.keys.to_a.sample(2)

hash_specials.each do |item|
  sale = (1..4).to_a.sample
  puts "Our specials of the day are #{item}! They are only $#{sale}!"
end
