puts "Holy giraffes! You fell into a maze!"
print "Where to? (N, E, S, W, NW, SW, NE, SE): "
direction = gets.chomp

if direction == "N"
  place = "North"
elsif direction == "E"
  place = "East"
elsif direction == "S"
  place = "South"
elsif direction == "W"
  place = "West"
elsif direction == "NW"
  place = "North West"
elsif direction == "SW"
  place = "South West"
elsif direction == "NE"
  place = "North East"
elsif direction == "SE"
  place = "South East"
end

puts "#{place}, you say? A fine choice!"

if place == "North"
  puts "You are in a maze of twisty little passages, all alike."
elsif place == "East"
  puts "You see an elf! And his pet ham!"
elsif place == "South"
  puts "There's a minotaur! Wait, no, that's just your reflection."
elsif place == "West"
  puts "You're here, wherever here is."
elsif place == "North West"
  puts "Ahh! You have fallen into a hole!"
elsif place == "South West"
  puts "You see a proper young lad and his pet rock."
elsif place == "North East"
  puts "There are a thousand squids! All at once!"
elsif place == "South East"
  puts "You are in a slide."
else
  puts "Wait, is that even a direction?"
end

print "Where to next? (Forward/Backward, Up/Down): "
movement = gets.chomp

puts "#{movement}, you say? A fine choice!"

if movement == "Forward"
  puts "You see a fire."
elsif movement == "Backward"
  puts "You have found a mountain of pebbles."
elsif movement == "Up"
  puts "You are on top of a hedge. Good job!"
elsif movement == "Down"
  puts "Don't lay down. You can do it. You just have to believe!"
else
  puts "Wait, is that even a direction?"
end
