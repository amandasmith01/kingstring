puts 'You stand on edge of a precipice whose depth disappears into the pitch black
shadows. You get a great idea. You walk closer to the drop and lean over the
edge, spitting into the depths just because you can. You laugh, obviously,
because that was hilarious. As you giggle you hear a crack. Suddenly the ground
gives way and you find yourself falling, likely to your death. You scream. And
flail. And (allegedly) cry for your mommy.'
puts 'You\'re still falling'
puts 'Still falling'
puts 'Stilll falling'
puts 'You hit the ground with a loud FWOOMP'
puts 'And you\'re... not dead??'
puts 'As you sit up and look around you find yourself in a circular room. There
are passageways to the north, south, east, and southwest. The one to the north
is very narrow but seems to be well lit. The one to the south is dark and eerie looking.
You can faintly hear a slow jazz song echoing from the passageway. It gives you
goosebumps. The one to the east is wide and clear with delicate hanging lights
guiding the way. The one to the southwest looks really boring. Which way should
we go? North, south, east, or southwest?'
# other directions : up(spiral staircase) down(trap door)

def first_move
  direction = gets.chomp.downcase

  if direction == "north"
    puts "You find yourself in a smallish room filled with dusty old books. There
is a leather armchair in the corner. It's falling apart but still looks fairly
comfy. There are beautiful paintings decorating the walls, some of which are
faded and peeling. There is also an ancient war sword mounted on the wall to
your left. You look around, pick up a book, and dust it off. The book is
colored a deep maroon with ugly tan patches where some inconsiderate bugs have
eaten away at the cover. Delicate gilded script embellishes the cover,
spelling out the words 'Raven's Guide to the Treasure of Morr'."


def book_choice
  puts 'Do you want to read the book?'
  choice = gets.chomp.downcase
  if choice == 'yes'
    puts "Intrigued, you carefully sit down on the rickety chair, which creaks
but holds, and begin to read. The pages tell of a girl named Raven who is
pulled through a book to the magical world of Morr. She meets a prince named
Nylian who is traversing the perilous Forbidden Forest with his best friend
Katal and a scorceress named Fioria in search of a legendary artifact. Raven is
swept up in a wirlwind of danger, blood, adventure, and love. She ends up saving a kingdom, a friend,
and herself. She learns much of the world and its intricacies, how it fits together
and how it falls apart. She learns about people, about life, about loss, about sacrifice.
Eventually, years later, she decides to write a book on her adventures, including a map of the
land of Morr and a piece of advice. Written on the very last page are the words
'To fear fear is to fear one's own self. Fear is not inherently bad. It can protect
and preserve. But to let fear prevent growth is to let one's own courage corrode.
Courage is as important as breath. It is the ability, not to discount fear, but to live beside it.
The ability to exist with. Do not run from fear. For it will always follow.
Accept it and move through life with the courage to fear.' The words echo in
your head as you slowly close the book. On the back cover of the book you notice an
intracite symbol with an indeciferable language weaved into the design. Above
the symbol lies the words 'speak these words to change your fate'. Suddenly your
brain hazzes over. Dizzy, you sway slightly, the chair quitely whining in complaint.
Just as suddenly as the disruption occured it disappears. As you try to reorient yourself,
you glance down at the book again, only to stare in bewilderment. Somehow you can
read the previously indecipherable text easily. Mystified, you whisper the words to yourself.
The language feels odd to your own ears. Foreign words in your own voice, coming from your own mouth,
rolling off your own tounge. As you utter the last word the symbol starts to glow.
Your world spins. Litterally. Violently. Everything bends and shifts, your universe suddenly fluid
as it slips through your fingers. As your conciousness fades, you see a blurry shape and two peircing
orbs of light. You can just make out its form but somehow you just know. It was a
wolf with fur as black as night and two glowing ice-blue eyes.

You blink. Blink again. Damn the sun is bright. Wait. You shoot up to a sitting
position. Bad idea. Your head spins and a searing pain hits you like a truck to
the face. A whine escapes your mouth as you put your head in your hands, covering your eyes.
Nausea overtakes you and you taste bile in the back of your throat. You manage to
keep it down but just barely. It feels like someone took a sledge hammer to the
back of your head but then desided that that wasn't enough so then proceeded to
kick you repeadedly in the stomach. After a long while the pain slowly fades to a
persistant dull ache that rests somewhere at the base of your skull. Your
brain, slightly more funtional now, finally begins to register your surroundings. You are
sitting on a strip of dew covered grass accompanying a dirt path leading north of you.
To your right tall trees block your veiw. A uuhhh. What was that word again?
Forest, your brain tells you. Right, a forest. You're still a tad woosie.
You look to your left and stare, eyes glued to the scene in front of you.
You gape in awe at a beutiful veiw of a castle. To the left of the path the green grass
gently slopes down, and out of veiw, revealing a magical sight.
The castle rests on a island in the middle of a small lake.
A picturesque waterfall drops off of a towering cliff covered in beautiful green trees,
a paint stroke of white and light aquamarine that
diaspears behind the massive castle. The castle itself is a sight to behold. Tall and white with
lofty peaks and curving archways, it sparkles like something out of a fairytale.
Unfortunately, as a result of all of your gasping you find you have to cough.
Your head apparently does not like coughing because
the pain returns, full force. You groan. Why again? Whyyy? When the pain subsides,
you catch movement out of the corner of your eye. Looking up you see a horse drawn
carriage coming towards you. Not really sure what else to do you just stay where you
are. As the carriage comes closer, it begins to slow, eventually stopping near
to where you sit. The shiny black door opens revealing a beautiful woman wearing
a pair of somewhat loose fitting pants tucked into a pair of knee high leather boots.
Her leather armor breatplate is covered by a clothe covering that drapes to the
side, leaving her arms bare. Leather arm bands adorne her biceps and an intracite
tatoo crawls up from her right hand ending just above her elbow. Her jet black
hair spills over her sholders in gentle ringlets and her bright green eyes twinkle
down at you. She smiles and offeres you a hand. You cautiously extent your hand
and she helps you to your feet. You withdraw your hand and take a step back,
concious of the fact you don't know this woman and still in the process of
processing all that has happened.
'Hello' she says, her voice as soft as silk and as warm as the summer sun,
'Are you alright?'
Your brain still kind of hazy, you take a moment to answer.
'Yes, yeah, yes, I'm fine thanks.' You pause, 'Um. Okay so, weird question but, where am I?'
Her eyebrows drift together in confusion.
'Where are you? You are in the Kingdom of Ile of course.'
'The Kingdom of Ile?' you ask.
'Yes. The second most powerful and by far the most peaceful kingdom in all
of Morr. Are you not a child of Ile?'
Your breath catches.
'What did you just say?'
She gives you a curious look.
'I asked whether you were a child of Ile.'
'No before that. Did you say Morr? As in the land of Morr?'
The woman looks very confused at this point.
'Um, uhh-yes?'
No. No way. Morr. The book. Raven's Guide to the Treasure of Morr. Morr. No way.
No. It's not possible. This is a dream. You passed out. You must have hit your head
or something. This can't be real. It just can't be.
'Morr' you whisper under your breath.
Slowly, your eyes scan your surroundings once more, taking it all in.
You guess the book was right. Your fate had indeed been changed.
Forever."
elsif choice == 'no'
  puts 'You carefully place the book on the ground, careful not to damage it
any further. Now what though? Should you read a different book, inspect the war
sword, or take a closer look at the paintings?'

  def after_book_choices

      three_main_choices = gets.chomp.downcase
  if (three_main_choices == "read a different book" || three_main_choices == "different book")
    puts "journal, thick, small"

    def read_a_different_book
      puts "choices...make sure the transition is in the prev funtion"
      #dusty very small book (ends up being small tan book), dusty very thick book
      # # (end up being green book), Dusty oddly shapped as in u can see som sort of
      # # something like buckle or wrapping or something hard to open (ends up being
      # # old journal)
      book = gets.chomp.downcase
      if book == "journal shape"
        puts "journal.."

    elsif book == "thick shape"
      puts "green book"

    elsif book == "small shape"
      puts "small tan book"
    else
      puts "9"
      read_a_different_book
    end
    end
    read_a_different_book

  elsif (three_main_choices == "inspect the war sword" || three_main_choices == "sword")
    puts "swing,put back,balance on nose"

    def sword
      do_w_sword = gets.chomp.downcase
      if do_w_sword == 'swing it around'
        puts "3"
        #functions
      elsif do_w_sword == 'puts it back'
        puts "4"
        #functions
      elsif do_w_sword == 'balance it on your nose'
        puts "death"
      else
        puts "6"
        sword
      end
    end
    sword
  elsif (three_main_choices == "take a closer look at the paintings" || three_main_choices == "paintings")
    puts "."
    # function take closer look at paintings
  else
    puts "Try typing one of the choices."
    after_book_choices
  end
  end
  after_book_choices

    else
      puts "dude it's a yes or no question its not that hard."
      book_choice
end
end

#dusty very small book (ends up being small tan book), dusty very thick book
# (end up being green book), Dusty oddly shapped as in u can see som sort of
# something like buckle or wrapping or something hard to open (ends up being
# old journal)
book_choice

  elsif direction == "south"
    puts "As you walk down the passage the music gets louder and louder, echoing
off the walls. You reach the end and find yourself in a small room. The walls
and ceiling are hidden behind large velvet drapes the color of blood and inky
black, the floors tiled with the colors of night. In the center of the
room lies a grand piano with a beautiful antique phonograph playing a smooth
jazz record. Sitting at the piano, playing along to the music, is a man wearing
a black suit and a dark red tie. His eyes remain closed as his fingers dance
upon the ivory keys. Feeling guilty for intruding on this moment you turn to
leave the room, only to find that the entrance is nowhere to be seen. Confused
and honestly a little scared, you turn back around to face the man. His fingers
slow their dance until they finally rest gracefully upon the keys. But you
hardly notice. You are far too distracted by his eyes. They pierce through you,
the blood red irises paralizing you.
'Hello' He says, his voice like velvet.
'Hello' you say, not knowing how you were able to find your voice but grateful
you were able to respond, 'You play beautifully.'
He stands and begins to walk towards you. Honestly you start to lowkey freak out
because you don't know this man and he has red eyes and holy heck he's getting
close. He stops in front of you, bowing slightly and offers you his hand.
'Would you care to dance?' he asks.
You contemplate for a few seconds before placing your hand on his. I mean what
else are you going to do? There's no exit. Or entrance. Whatever. There's no
way out. And, truthfully, there was something intriguing about the man,
something captivating about him.
He pulls you towards the middle of the room and the two of you begin to waltz."
  elsif direction == "east"
    puts "As you walk along the passageway you hear lively music and see beautiful
lights flashing. You reach the end of the corridor to see a large room filled
with people. All the colors of the rainbow dance in the air, and festive music
plays at max volume. The room is filled with colorful decorations; There is a
dance floor and a DJ Everyone is dancing "
  # a party and it turns out to be your birthay party. function choices.

  elsif direction == "southwest"
    puts "12"
# a never ending hallway. other interesting stuff. function choices.
  else
    puts 'Ummmm...uhh. Bro. Weren\'t you listening? There\'s no path that way. Dunce.'
"Try picking a direction this time."
first_move
end
end

first_move

#
# def sword
#   gets.chomp.downcase
#   if 'swing it around'
#     puts ""
#   elsif 'puts it back'
#     puts ""
#   elsif 'balance it on your nose'
#     puts ""
#   else
#     puts ""
#     sword
#   end
# end
# sword
# change into fucntion format so I can do multiple choices lie zork. look at
# the sheets you did w steve



# #dusty very small book (ends up being small tan book), dusty very thick book
# # (end up being green book), Dusty oddly shapped as in u can see som sort of
# # something like buckle or wrapping or something hard to open (ends up being
# # old journal)
